#!/usr/bin/env python3
import cgi
import logging

form = cgi.FieldStorage()
text1 = form.getfirst("email", "не задано")

logging.basicConfig(filename="/tmp/sample.log", level=logging.INFO)

logging.info("{}".format(text1))

print("Content-type: text/html\n")
print("""<!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Обработка данных форм</title>
        </head>
        <body>""")

print("<h1>Ваш email {} успешно отвязан</h1>".format(text1))

#print("<h1>Спасибо, что перешли по моей ссылке!</h1>")
#print("<p>Если хотите отписаться от рассылки, то введите ваш email</p>")

print("""</body>
        </html>""")