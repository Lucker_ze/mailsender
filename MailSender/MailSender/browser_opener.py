from selenium import webdriver
import openpyxl 
import openpyxl.styles
from openpyxl import Workbook
import json
import time
import random
import threading 
import copy


def send_keys_delay(browser_element, string):
    for letter in string:
        browser_element.send_keys(letter)
        time.sleep(0.18 + random.randint(0,5)/100)
    return

def auth(browser, login, password):
    # Переход на страницу почты
    browser.get('https://account.mail.ru/')
    time.sleep(5)
    #browser.maximize_window()
    browser.implicitly_wait(50)

    email_text_field = browser.find_element_by_css_selector('#root > div > div.login-panel > div > div > div > form > div:nth-child(2) > div > div.login-row.username > div > div > div > div > div > div.base-1-1-61.first-1-1-66 > div > input')
    send_keys_delay(email_text_field, login)
    go_btn = browser.find_element_by_css_selector('#root > div > div.login-panel > div > div > div > form > div:nth-child(2) > div > div:nth-child(3) > div > div:nth-child(1) > button')
    go_btn.click()
    browser.implicitly_wait(50)
    password_text_field = browser.find_element_by_css_selector('#root > div > div.login-panel > div > div > div > form > div:nth-child(2) > div > div.login-row.password > div > div > div > div > div > input')
    #browser.execute_script("arguments[0].focus();", password_text_field)
    time.sleep(3)
    send_keys_delay(password_text_field, password)
    
    return

def work_thread(chromedriver, e_login, e_password, proxy):  
    options = webdriver.ChromeOptions()
    #options.add_argument('--log-level=3') #чтобы не выводить дебаг инфу
    #options.add_argument('--headless')  # для открытия headless-браузера
    options.add_argument('--no-sandbox')
    options.add_argument("--window-size=1920,1080")
    if proxy != '' :
        options.add_argument('--proxy-server=' + proxy)
    browser = webdriver.Chrome(executable_path=chromedriver, chrome_options=options)	

    browser.implicitly_wait(50)
    auth(browser, e_login, e_password)

    return

#---MAIN---#
json_config = json.load(open('./config.json', 'r', encoding='utf-8')) 
Proxys = list(json_config['proxy list'])
if int(json_config['threads count']) > len(Proxys) or len(Proxys) < 1:
    print('Недостаточно прокси для выбранного количества потоков!')
    time.sleep(6)
    quit()

# путь к драйверу chrome
chromedriver = './templates/chromedriver.exe'
#chromedriver = './templates/chromedriver'

json_emails = json.load(open('./emails.json', 'r', encoding='utf-8'))
e_logins = list(json_emails['logins_list'])
e_passwords = list(json_emails['passwords_list'])

threads = []

send_count = int(json_config['threads count'])
open_e_number = -2
print('Type e-mail number or -1 for exit')
open_e_number = int(input())
while(open_e_number != -1):
    threads.append(threading.Thread(target = work_thread, args = (chromedriver, e_logins[open_e_number], e_passwords[open_e_number], Proxys[open_e_number], )))
    threads[-1].start()
    threads[-1].join()
    print('Type e-mail number or -1 for exit')
    open_e_number = int(input())

print('success')
