import openpyxl 
import openpyxl.styles
from openpyxl import Workbook
import sqlite3

class Person(object):
       def __init__(self, inn, company_name, first_name, last_name, second_name, phone, email, banned = False):
              self.inn = inn
              self.company_name = company_name
              self.first_name = first_name
              self.last_name = last_name
              self.second_name = second_name
              self.phone = phone
              self.email = email
              self.banned = banned
       
       def to_list(self):
              return (self.inn, self.company_name, self.first_name, self.last_name, self.second_name, self.phone, self.email, self.banned)

#---MAIN---#
conn = sqlite3.connect("./emails_db.db")
cursor = conn.cursor()

#cursor.execute("""SELECT CASE WHEN inn == '111'
#       THEN 'EXISTS'
#       ELSE 'value abcd is missing'
#       END AS PasswordPresent
#       FROM Persons;""")
#print(cursor.fetchall())

DB_dirty_wb = openpyxl.load_workbook('./work_dir/Новая e-mail база.xlsx')
DB_dirty = DB_dirty_wb.active

DB_clear_wb = openpyxl.load_workbook('./tmp/Рассылка e-mail база.xlsx')
DB_clear = DB_clear_wb.active
DB_clear.delete_rows(1,DB_clear.max_row+1)
DB_clear_wb.save('./tmp/Рассылка e-mail база.xlsx')

Persons_list = []

for i in range(2,DB_dirty.max_row):
       fio = str(DB_dirty.cell(row = i, column = 3).value).split()
       if len(fio) != 3 or DB_dirty.cell(row = i, column = 1).value == None or DB_dirty.cell(row = i, column = 5).value == None:
              continue
       Persons_list.append(Person(
              DB_dirty.cell(row = i, column = 1).value,
              DB_dirty.cell(row = i, column = 2).value,
              fio[1],
              fio[0],
              fio[2],
              DB_dirty.cell(row = i, column = 4).value,
              DB_dirty.cell(row = i, column = 5).value,
              False
       ))

added_cnt = 0
skiped_cnt = 0
banned_cnt = 0

for person in Persons_list:
       cursor.execute("""SELECT * FROM Persons WHERE inn=?;""", [(person.inn)])
       ans = cursor.fetchall()
       if len(ans) == 0:
              persons_data = person.to_list()
              cursor.execute("""INSERT INTO Persons (inn, company_name, first_name, last_name, second_name, phone, email, banned)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?);""", persons_data)
              cur_row = DB_clear.max_row+1
              for i in range(len(persons_data)):
                     DB_clear.cell(row = cur_row, column = i+1).value = persons_data[i]
              
       if len(ans) == 1:
              if ans[0][7] == 0:
                     skiped_cnt += 1
              else:
                     banned_cnt += 1
conn.commit()

DB_clear_wb.save('./tmp/Рассылка e-mail база.xlsx')
print('added = ' + str(added_cnt))
print('skiped = ' + str(skiped_cnt))
print('banned = ' + str(banned_cnt))
