import sqlite3

conn = sqlite3.connect("./emails_db.db")
cursor = conn.cursor()
print('Type email for ban:')

cursor.execute("""UPDATE Persons SET banned=1 WHERE email LIKE ?;""", [('%' +input() + '%')])
conn.commit()

cursor.execute("""SELECT * FROM Persons WHERE banned=1;""")
print(cursor.fetchall())