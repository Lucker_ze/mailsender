from selenium import webdriver
import openpyxl 
import openpyxl.styles
from openpyxl import Workbook
import json
import time
import random
import threading 
import copy
import datetime

def get_io_string(FIO):
    fio_list = FIO.split()
    io = fio_list[0] + ' aa'
    if len(fio_list) > 1:
        if  fio_list[1] != '':
            io += fio_list[1].title() 
        if fio_list[2] != '':
            io += ' ' + fio_list[2].title()
    return io

def send_keys_delay(browser_element, string):
    for letter in string:
        browser_element.send_keys(letter)
        time.sleep(0.18 + random.randint(0,5)/100)
    return

def auth(browser, login, password):
    # Переход на страницу почты
    browser.get('https://account.mail.ru/')
    time.sleep(5)
    #browser.maximize_window()
    browser.implicitly_wait(50)

    email_text_field = browser.find_element_by_css_selector('#root > div > div.login-panel > div > div > div > form > div:nth-child(2) > div > div.login-row.username > div > div > div > div > div > div.base-1-1-61.first-1-1-66 > div > input')
    send_keys_delay(email_text_field, login)
    go_btn = browser.find_element_by_css_selector('#root > div > div.login-panel > div > div > div > form > div:nth-child(2) > div > div:nth-child(3) > div > div:nth-child(1) > button')
    go_btn.click()
    browser.implicitly_wait(50)
    password_text_field = browser.find_element_by_css_selector('#root > div > div.login-panel > div > div > div > form > div:nth-child(2) > div > div.login-row.password > div > div > div > div > div > input')
    #browser.execute_script("arguments[0].focus();", password_text_field)
    time.sleep(3)
    send_keys_delay(password_text_field, password)
    
    go_btn = browser.find_element_by_css_selector('#root > div > div.login-panel > div > div > div > form > div:nth-child(2) > div > div:nth-child(3) > div > div:nth-child(1) > div > button')
    go_btn.click()
    return

def send_mail(browser, email):
    browser.implicitly_wait(50)
    go_btn = browser.find_element_by_css_selector('#app-canvas > div > div.application-mail > div.application-mail__overlay > div > div.application-mail__layout.application-mail__layout_main > span > div.layout__main-frame > div > div > div > div > div.letter-list.letter-list_has-letters > div > div > div.dataset-letters > div > div > div > a.llc.js-tooltip-direction_letter-bottom.js-letter-list-item.llc_normal.llc_last')
    go_btn.click()

    time.sleep(1)
    browser.implicitly_wait(50)
    try:
        text_field = browser.find_element_by_css_selector('body > div.compose-windows > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__compose > div.container--rp3CE > div.scrollview--SiHhk.scrollview_main--3Vfg9 > div.head_container--3W05z > div > div > div.wrap--2sfxq > div > div.contacts--1ofjA > div > div > label > div > div > input')
        send_keys_delay(text_field, email)
    except BaseException:   
        try:
            text_field = browser.find_element_by_css_selector('body > div.compose-windows > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__compose > div.container--rp3CE > div.scrollview--SiHhk.scrollview_main--3Vfg9 > div.head_container--3W05z > div > div > div.wrap--2sfxq > div > div.contacts--1ofjA > div > div > label > div > div > input')
            send_keys_delay(text_field, email)
        except BaseException:
            print('PIPEZ')

    go_btn = browser.find_element_by_css_selector('body > div.compose-windows > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__footer > div.compose-app__buttons > span.button2.button2_base.button2_primary.button2_hover-support.js-shortcut > span')
    go_btn.click()
    return

def work_thread(chromedriver, e_login, e_password, proxy, emails, names):  
    options = webdriver.ChromeOptions()
    #options.add_argument('--log-level=3') #чтобы не выводить дебаг инфу
    options.add_argument('--headless')  # для открытия headless-браузера
    options.add_argument('--no-sandbox')
    options.add_argument("--window-size=1920,1080")
    if proxy != '' :
        options.add_argument('--proxy-server=' + proxy)
    browser = webdriver.Chrome(executable_path=chromedriver, chrome_options=options)	

    browser.implicitly_wait(50)
    auth(browser, e_login, e_password)
    go_btn = browser.find_element_by_css_selector('#sideBarContent > div > nav > a:nth-child(7)')
    go_btn.click()
    time.sleep(3)

    for i in range(0,len(emails)):
        try:
            send_mail(browser,emails[i])
            print('thread ' + str(e_login) +' ' + str(datetime.datetime.now()) + ': ' + 'mail ' + str(i) + ' sended. ')
        except BaseException:
            try:
                exit_btn = browser.find_element_by_css_selector('body > div.overlay--2THpd > div > div > div.c1110 > button')
                exit_btn.click()
                exit_btn = browser.find_element_by_css_selector('body > div.compose-windows > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_adaptive > div > div.compose-app__compose > div.container--rp3CE > div.controls_container--17SRg > div > div > button:nth-child(3)')
                exit_btn.click()
                print('thread ' + str(e_login) + ' ' + str(datetime.datetime.now()) + ': ' + 'mail ' + str(i) + ' wrong mail. ' )
            except BaseException:
                time.sleep(60)
                browser.get('https://e.mail.ru/templates/')
                print('thread ' + str(e_login) + ' ' + str(datetime.datetime.now()) + ': ' + 'mail ' + str(i) + ' I don\'t know what is going wrong!. ' )
        time.sleep(30+random.randint(0,30))
    return

#---MAIN---#
json_config = json.load(open('./config.json', 'r', encoding='utf-8')) 
Proxys = list(json_config['proxy list'])
if int(json_config['threads count']) > len(Proxys) or len(Proxys) < 1:
    print('Недостаточно прокси для выбранного количества потоков!')
    time.sleep(6)
    quit()

# путь к драйверу chrome
#chromedriver = './templates/chromedriver.exe'
chromedriver = './templates/chromedriver'

threads = []

json_emails = json.load(open('./emails.json', 'r', encoding='utf-8'))
e_logins = list(json_emails['logins_list'])
e_passwords = list(json_emails['passwords_list'])

emails_for_send_wb = openpyxl.load_workbook('./work_dir/Рассылка e-mail база.xlsx')
emails_for_send = emails_for_send_wb.active
cur_line = 2

send_count = 2
for i in range(send_count):
    emails = list()
    names = list()
    for id in range(cur_line,cur_line+(emails_for_send.max_row-1)//send_count):
        emails.append(emails_for_send.cell(row = id, column = 5).value)
        names.append(get_io_string(emails_for_send.cell(row = id, column = 3).value))
        cur_line+=1
    threads.append(threading.Thread(target = work_thread, args = (chromedriver, e_logins[i], e_passwords[i], Proxys[i], emails, names,  )))
    threads[-1].start()
    print('thread ' + str(i) + ' started with ' + str(len(emails)) + ' e-amils')
for thread in threads:
    thread.join()

print('success')
input()